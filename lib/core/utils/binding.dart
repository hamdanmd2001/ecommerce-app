import 'package:commerce_app/core/controller/cart_controller.dart';
import 'package:commerce_app/core/controller/explore_screen_controller.dart';
import 'package:get/get.dart';
import '../controller/auth_controller.dart';
import '../controller/home_screen_controller.dart';
import '../controller/product_details_controller.dart';
import '../controller/profile_controller.dart';

class Binding extends Bindings{
  @override
  void dependencies() {
  Get.lazyPut(() => AuthController());
  Get.lazyPut(() => HomeScreenController());
  Get.lazyPut(() => ExploreScreenController());
  Get.lazyPut(() => CartController(), fenix: true);
  Get.lazyPut(() => ProductDetailsController() , fenix: true);
  Get.lazyPut(() => ProfileController());
  }
}