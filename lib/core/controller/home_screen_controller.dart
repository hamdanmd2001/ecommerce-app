import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../view/cart_screen.dart';
import '../view/explore_screen.dart';
import '../view/profile_screen.dart';

class HomeScreenController extends GetxController {
  int _navBarValue = 0;
  Widget _currentScreen = ExploreScreen();

  get navBarValue => _navBarValue;
  get currentScreen => _currentScreen;

  void changeSelectedValue(int selectedValue) {
    _navBarValue = selectedValue;
    switch (selectedValue) {
      case 0:
        _currentScreen = ExploreScreen();
        break;

      case 1:
        _currentScreen = CartScreen();
        break;

      case 2:
        _currentScreen = ProfileScreen();
        break;
    }
    update();
  }
}
