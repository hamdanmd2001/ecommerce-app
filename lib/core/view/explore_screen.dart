import 'package:cached_network_image/cached_network_image.dart';
import 'package:commerce_app/global/custom_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../global/theme/app_colors/app_light_colors.dart';
import '../controller/explore_screen_controller.dart';
import 'widgets/custom_text.dart';

class ExploreScreen extends StatelessWidget {
  const ExploreScreen({super.key});
  @override
  Widget build(BuildContext context) {
    ExploreScreenController exploreScreenController = Get.find();
    return GetBuilder<ExploreScreenController>(
      init: exploreScreenController,
      builder: (controller) => controller.loading.value
          ? const Center(child: CircularProgressIndicator())
          : Scaffold(
              body: Container(
                padding: EdgeInsets.fromLTRB(
                  Get.width * .075,
                  Get.height * .075,
                  Get.width * .075,
                  Get.height * .0,
                ),
                child: Column(
                  children: [
                    _searchTextFormField(),
                    SizedBox(
                      height: Get.height * .05,
                    ),
                    const CustomText(
                        text: 'Categories', style: CustomTextStyle.mediumBold),
                    SizedBox(
                      height: Get.height * .04,
                    ),
                    _listViewCategories(),
                    SizedBox(
                      height: Get.height * 0.05,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        CustomText(
                            text: 'Best Selling',
                            style: CustomTextStyle.mediumBold),
                        CustomText(
                          text: 'See all',
                          style: CustomTextStyle.medium,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: Get.height * 0.04,
                    ),
                    _listViewBestSellingProducts(),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _searchTextFormField() {
    return Container(
      decoration: BoxDecoration(
        color: AppLightColors.captionTextColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: TextFormField(
        decoration: const InputDecoration(
          prefixIcon: Icon(Icons.search),
          border: InputBorder.none,
        ),
      ),
    );
  }

  Widget _listViewCategories() {
    return GetBuilder<ExploreScreenController>(
      init: ExploreScreenController(),
      builder: (controller) => SizedBox(
        height: Get.height * .12,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: controller.categories.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Container(
                  height: Get.height * .075,
                  width: Get.width * .185,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    shape: BoxShape.circle,
                  ),
                  child: CachedNetworkImage(
                    progressIndicatorBuilder: (context, url, progress) =>
                        Center(
                      heightFactor: .2,
                      child: CircularProgressIndicator(
                        value: progress.progress,
                      ),
                    ),
                    imageUrl: controller.categories[index].image!,
                  ),
                ),
                SizedBox(
                  height: Get.height * .02,
                ),
                CustomText(
                  text: controller.categories[index].name!,
                  style: CustomTextStyle.small,
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _listViewBestSellingProducts() {
    return GetBuilder<ExploreScreenController>(
      init: ExploreScreenController(),
      builder: (controller) => SizedBox(
        height: Get.height * .43,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemCount: controller.bestSellingProducts.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Get.toNamed('/details',
                    arguments: controller.bestSellingProducts[index]);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: Get.height * .3,
                    width: Get.width * .4,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      shape: BoxShape.rectangle,
                    ),
                    child: CachedNetworkImage(
                      fit: BoxFit.fill,
                      progressIndicatorBuilder: (context, url, progress) =>
                          Center(
                        heightFactor: .2,
                        child: CircularProgressIndicator(
                          value: progress.progress,
                        ),
                      ),
                      imageUrl: controller.bestSellingProducts[index].image!,
                    ),
                  ),
                  SizedBox(
                    height: Get.height * .012,
                  ),
                  CustomText(
                    text: controller.bestSellingProducts[index].name ?? '',
                    style: CustomTextStyle.small,
                  ),
                  SizedBox(
                    height: Get.height * .01,
                  ),
                  SizedBox(
                    height: Get.height * .02,
                    child: CustomText(
                      text: controller.bestSellingProducts[index].description ??
                          '',
                      style: CustomTextStyle.captionText,
                      maxLines: 1,
                    ),
                  ),
                  SizedBox(
                    height: Get.height * .01,
                  ),
                  CustomText(
                      text: '${controller.bestSellingProducts[index].price}\$',
                      style: CustomTextStyle.smallPrimary),
                ],
              ),
            );
          },
          separatorBuilder: (context, index) => SizedBox(
            width: Get.width * 0.05,
          ),
        ),
      ),
    );
  }
}
