import 'package:commerce_app/core/controller/profile_controller.dart';
import 'package:commerce_app/core/view/widgets/custom_profile_button.dart';
import 'package:commerce_app/global/custom_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/custom_text.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ProfileController>(
        init: ProfileController(),
        builder: (controller) {
          return Container(
            padding: EdgeInsets.fromLTRB(
              Get.width * .05,
              Get.height * .075,
              Get.width * .05,
              Get.height * .075,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      height: Get.height * .15,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(90),
                      ),
                      child: Image.asset(
                        'assets/images/Avatar.png',
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        CustomText(
                          text: 'Hamdan',
                          style: CustomTextStyle.xLarge,
                        ),
                        CustomText(
                          text: 'iamhamdan@gmail.com',
                          style: CustomTextStyle.small,
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: Get.height * .18,
                ),
                const CustomProfileButton(
                  iconPath: 'assets/images/Icon_Edit-Profile.png',
                  title: 'Edit Profile',
                ),
                SizedBox(
                  height: Get.height * .04,
                ),
                CustomProfileButton(
                  iconPath: 'assets/images/Icon_Location.png',
                  title: 'Shipping Address',
                  onTap: () {},
                ),
                SizedBox(
                  height: Get.height * .04,
                ),
                CustomProfileButton(
                  iconPath: 'assets/images/Icon_History.png',
                  title: 'Order History',
                  onTap: () {},
                ),
                SizedBox(
                  height: Get.height * .04,
                ),
                CustomProfileButton(
                  iconPath: 'assets/images/Icon_Payment.png',
                  title: 'Cards',
                  onTap: () {},
                ),
                SizedBox(
                  height: Get.height * .04,
                ),
                CustomProfileButton(
                  iconPath: 'assets/images/Icon_Alert.png',
                  title: 'Notifications',
                  onTap: () {},
                ),
                SizedBox(
                  height: Get.height * .04,
                ),
                CustomProfileButton(
                  iconPath: 'assets/images/Icon_Exit.png',
                  title: 'Log Out',
                  onTap: () => controller.signOut(),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
