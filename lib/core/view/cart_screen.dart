import 'package:cached_network_image/cached_network_image.dart';
import 'package:commerce_app/core/controller/cart_controller.dart';
import 'package:commerce_app/core/view/widgets/custom_text.dart';
import 'package:commerce_app/global/custom_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'widgets/custom_flat_button.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartController>(
      builder: (controller) =>
      controller.loading.value == true ?
     const Center(child: CircularProgressIndicator(),)
      :
      controller.cartProducts.isEmpty
          ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/svg/empty_cart.svg',
                alignment: Alignment.topCenter,
                height: Get.height * .3,
              ),
              Container(
                padding: EdgeInsets.all(Get.width * .2),
                child: const CustomText(
                  text: 'Your Cart Is Currently Empty',
                  style: CustomTextStyle.largeBold,
                ),
              ),
            ],
          )
          : Scaffold(
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(
                        Get.width * .05,
                        Get.height * .05,
                        Get.width * .05,
                        Get.height * .0,
                      ),
                      child: ListView.separated(
                        itemCount: controller.cartProducts.length,
                        itemBuilder: (ctx, index) {
                          return SizedBox(
                            height: Get.height * .15,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: Get.width * .35,
                                  height: Get.height,
                                  child: CachedNetworkImage(
                                    fit: BoxFit.fill,
                                    progressIndicatorBuilder:
                                        (context, url, progress) => Center(
                                      heightFactor: .2,
                                      child: CircularProgressIndicator(
                                        value: progress.progress,
                                      ),
                                    ),
                                    imageUrl:
                                        controller.cartProducts[index].image!,
                                  ),
                                ),
                                SizedBox(
                                  width: Get.width * .1,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: Get.height * .02,
                                    ),
                                    CustomText(
                                      text:
                                          controller.cartProducts[index].name!,
                                      style: CustomTextStyle.medium,
                                    ),
                                    SizedBox(
                                      height: Get.height * .001,
                                    ),
                                    CustomText(
                                      text:
                                          '\$${controller.cartProducts[index].price!}',
                                      style: CustomTextStyle.mediumPrimary,
                                    ),
                                    SizedBox(
                                      height: Get.height * .02,
                                    ),
                                    Container(
                                      height: Get.height * .04,
                                      width: Get.width * .3,
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade100,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          GestureDetector(
                                            child: const Icon(
                                              Icons.add,
                                            ),
                                            onTap: () {
                                              controller.incQty(index);
                                            },
                                          ),
                                          CustomText(
                                            text: controller
                                                .cartProducts[index].quantity
                                                .toString(),
                                            style: CustomTextStyle.medium,
                                            alignment: Alignment.center,
                                          ),
                                          GestureDetector(
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  bottom: Get.height * .02),
                                              child: const Icon(
                                                Icons.minimize,
                                              ),
                                            ),
                                            onTap: () {
                                              controller.decQty(index);
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(height: Get.height * .02),
                      ),
                    ),
                  ),
                  _buildButtom(),
                ],
              ),
            ),
    );
  }

  Widget _buildButtom() {
    return GetBuilder<CartController>(
      builder: (controller) => Container(
        padding: EdgeInsets.fromLTRB(
            Get.width * .05, Get.height * .01, Get.width * .05, 0),
        height: Get.height * .1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const CustomText(
                  text: 'TOTAL',
                  style: CustomTextStyle.captionText,
                ),
                CustomText(
                    text: '\$${controller.subTotal}',
                    style: CustomTextStyle.largePrimaryBold),
              ],
            ),
            Container(
              margin: EdgeInsets.only(bottom: Get.height * .0175),
              height: Get.height * .06,
              width: Get.width * .4,
              child: CustomFlatButton(
                text: 'CHECKOUT',
                onPressed: () async {
                  await controller.checkOut();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
