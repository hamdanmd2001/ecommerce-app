import 'dart:ffi';

import 'package:commerce_app/global/theme/app_colors/app_light_colors.dart';
import 'package:commerce_app/global/font_size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final Alignment? alignment;
  final int? maxLines;

  const CustomText({
    super.key,
    required this.text,
    this.style,
    this.alignment = Alignment.topLeft,
    this.maxLines,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      child: Text(
        text,
        style: style ?? Theme.of(context).textTheme.displayLarge,
        maxLines: maxLines ?? 10,
      ),
    );
  }
}
