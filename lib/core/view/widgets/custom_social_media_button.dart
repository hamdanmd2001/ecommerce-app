import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'custom_text.dart';

class CustomSocialMediaButton extends StatelessWidget {
  final String text;
  final String svgPath;
  final void Function()? onPressed;

  const CustomSocialMediaButton(
      {super.key, required this.text, required this.svgPath, this.onPressed});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double height = size.height;
    double width = size.width;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.grey.shade100,
      ),
      child: MaterialButton(
        height: height * 0.05,
        onPressed: onPressed,
        child: Row(
          children: [
            SvgPicture.asset(
              svgPath,
              height: height * 0.03,
              width: width * 0.015,
            ),
            SizedBox(
              width: width * 0.2,
            ),
            CustomText(
              text: text,
              style: Theme.of(context).textTheme.bodyMedium,
            )
          ],
        ),
      ),
    );
  }
}
