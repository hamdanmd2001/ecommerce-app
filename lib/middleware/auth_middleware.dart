import 'package:commerce_app/main.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/utils/locale_storage_data.dart';

class AuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? root) {
    if (sharedPrefs?.getString(cachedUserData) != null) {
      return const RouteSettings(name: '/home');
    }
  }
}
