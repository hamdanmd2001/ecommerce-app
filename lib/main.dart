import 'package:commerce_app/core/view/product_details_screen.dart';
import 'package:commerce_app/global/theme/light_theme.dart';
import 'package:commerce_app/middleware/auth_middleware.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/utils/binding.dart';
import 'core/view/login_screen.dart';
import 'core/view/sign_up_screen.dart';
import 'core/view/home_screen.dart';

SharedPreferences? sharedPrefs;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  sharedPrefs = await SharedPreferences.getInstance();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: getLightTheme(),
      initialBinding: Binding(),
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page: () => LoginScreen(),
          binding: Binding(),
          middlewares: [
            AuthMiddleware(),
          ],
        ),
        GetPage(
          name: '/home',
          page: () => const HomeScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/signup',
          page: () => SignUpScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/details',
          page: () => ProductDetailes(),
          binding: Binding(),
        ),
      ],
    );
  }
}
